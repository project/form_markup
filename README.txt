***********
* README: *
***********

DESCRIPTION:
------------
This module offers the possibility to add prefix and suffix 
values for each cck widget.


REQUIREMENTS:
-------------
The form_markup.module requires the content.module to be installed.


INSTALLATION:
-------------
1. Place the entire form_markup directory into your Drupal modules/
   directory.

2. Enable the form_markup module by navigating to:

     administer > modules
     

USING FORM_MARKUP:
-----------------
When creating a new or editing an existing field, there will appear a fieldset called Markup. 
Here you can set, what additional html markup will be added around your field. Both fields are
optional, so you can leave it empty or just fill in one or both. 
For example you can put a <div id="tel-number"> (prefix) and <div> (suffix) around for 
additional css-design.

Author:
-------
Matthias Hutterer
mh86@drupal.org
m_hutterer@hotmail.com